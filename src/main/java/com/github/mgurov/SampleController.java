package com.github.mgurov;

import com.github.mgurov.configuration.SelfClientConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableAutoConfiguration
@ComponentScan
public class SampleController {

    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello World!";
    }

    @RequestMapping("/boom")
    @ResponseBody
    String boom(
            @Value("${app.boom:\uD83C\uDF32}") String boom
    ) {
        return boom;
    }

    @RequestMapping("/proxied-boom1")
    @ResponseBody
    String proxiedBoom1() {
        return selfClient1.boom();
    }

    @RequestMapping("/proxied-boom2")
    @ResponseBody
    String proxiedBoom2() {
        return selfClient2.boom();
    }

    @Autowired
    private SelfClientConfiguration.SelfClient selfClient1;
    @Autowired
    private SelfClientConfiguration.SelfClient selfClient2;

    public static void main(String[] args) {
        SpringApplication.run(SampleController.class, args);
    }

}