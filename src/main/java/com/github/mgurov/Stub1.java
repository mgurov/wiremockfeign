package com.github.mgurov;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;

public class Stub1 extends ResponseDefinitionTransformer {
    public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource fileSource, Parameters parameters) {
        return WireMock.ok(getName()).build();
    }

    public String getName() {
        return "stub1";
    }

    @Override
    public boolean applyGlobally() {
        return false;
    }
}
