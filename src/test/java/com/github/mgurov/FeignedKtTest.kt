package com.github.mgurov

import com.fasterxml.jackson.annotation.JsonRootName
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.junit.WireMockRule
import feign.Feign
import feign.Logger
import feign.RequestLine
import feign.jackson.JacksonDecoder
import feign.slf4j.Slf4jLogger
import org.junit.Rule
import org.junit.Test

import com.github.tomakehurst.wiremock.client.WireMock.get
import feign.Param
import org.assertj.core.api.Assertions.assertThat

class FeignedKtTest {

    @get:Rule
    val wireMockRule = WireMockRule(WireMockConfiguration.options().dynamicPort())

    @Test
    fun testConnection() {

        wireMockRule.stubFor(
                get("/data/1").willReturn(WireMock.okJson("{\"root\": {\"field\": \"value\"}}"))
        )

        wireMockRule.stubFor(
                get("/data/2").willReturn(WireMock.okJson("{\"root\": {\"field\": \"z\"}}"))
        )

        wireMockRule.stubFor(
                get("/data").willReturn(WireMock.okJson("[{\"root\": {\"field\": \"value\"}}, {\"root\": {\"field\": \"z\"}}]"))
        )

        val client = Feign.builder()
                .logger(Slf4jLogger(FeignClient::class.java))
                .logLevel(Logger.Level.FULL)
                .decoder(JacksonDecoder(
                        ObjectMapper()
                                .registerModule(KotlinModule())
                                .enable(DeserializationFeature.UNWRAP_ROOT_VALUE)
                ))
                .target(FeignClient::class.java, "http://localhost:" + wireMockRule.port())

        assertThat(client.data(1).field).isEqualTo("value")
        val datas: List<Data> = client.datas()
        assertThat(datas).containsExactly(
                Data("field"), Data("z")
        )
    }

    interface FeignClient {
        @RequestLine("GET /data")
        fun datas(): List<Data>
        @RequestLine("GET /data/{id}")
        fun data(@Param("id") id: Int): Data
    }


    @JsonRootName("root")
    data class Data(
            val field: String
    )

}
