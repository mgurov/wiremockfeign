package com.github.mgurov;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class App {
    private static void main(String[] args) {
        Stub1 stub1 = new Stub1();
        Stub2 stub2 = new Stub2();
        WireMockServer wireMockServer = new WireMockServer(WireMockConfiguration.options()
                .extensions(stub1, stub2)
                .port(8080));
        wireMockServer.start();

        stubFor(any(urlEqualTo("/ping"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/plain")
                        .withBody("fine")
                )
        );

        stubFor(any(urlEqualTo("/stub1"))
                .willReturn(aResponse()
                        .withTransformers(stub1.getName())
                )
        );

        stubFor(any(urlEqualTo("/stub2"))
                .willReturn(aResponse()
                        .withTransformers(stub2.getName())
                )
        );

    }
}
