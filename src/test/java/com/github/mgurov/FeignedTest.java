package com.github.mgurov;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import feign.Feign;
import feign.Logger;
import feign.Param;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.slf4j.Slf4jLogger;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static org.assertj.core.api.Assertions.assertThat;

public class FeignedTest {

    @Rule
    public final WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.options().dynamicPort());

    @Test
    public void testConnection() {

        wireMockRule.stubFor(
                get("/data").willReturn(WireMock.okJson("[{\"field\": \"value\"}]"))
        );

        FeignClient client = Feign.builder()
                .logger(new Slf4jLogger(FeignClient.class))
                .logLevel(Logger.Level.FULL)
                .decoder(new JacksonDecoder())
                .target(FeignClient.class, "http://localhost:" + wireMockRule.port());

        assertThat(client.datas()).extracting("field").containsExactly("value");
    }

    interface FeignClient {
        @RequestLine("GET /data")
        List<Data> datas();
        @RequestLine("GET /data/{id}")
        Data data(@Param("id") int id);
    }

    public static class Data {
        public String field;
    }

}
