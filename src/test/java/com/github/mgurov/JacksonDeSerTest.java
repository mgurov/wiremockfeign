package com.github.mgurov;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

public class JacksonDeSerTest {

    @Test
    public void simplest() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Data data = objectMapper.readValue("{\"field\": \"value\"}", Data.class);
        assertThat(data.field).isEqualTo("value");
    }

    public static class Data {
        @JsonProperty
        String field;
    }

    @Test
    public void creator() throws IOException {
        ObjectReader reader = new ObjectMapper().readerFor(DataCreator.class);

        DataCreator data = reader.readValue("{\"id\":1,\"theName\":\"My bean\"}");

        assertThat(data.id).isEqualTo("1");
    }

    public static class DataCreator {
        final String id;
        final String name;

        @JsonCreator
        public DataCreator(
                @JsonProperty("id") String id,
                @JsonProperty("theName") String name) {
            this.id = id;
            this.name = name;
        }
    }

    @Test
    public void staticCreator() throws IOException {
        String json = "{\"id\":1,\"type\":\"My bean\"}";

        ObjectReader reader = new ObjectMapper().readerFor(StaticDataCreator.class);

        StaticDataCreator data = reader.readValue(json);

        assertThat(data.id).isEqualTo("1");
    }

    public static class StaticDataCreator {
        final String id;
        final String name;

        public StaticDataCreator(String id, String name) {
            this.id = id;
            this.name = name;
        }

        @JsonCreator
        public static StaticDataCreator create(
                @JsonProperty("id") String id,
                @JsonProperty("type") String name) {
            return new StaticDataCreator(id, name);
        }
    }

    @Test
    public void deRootName() throws IOException {
        String json = "{\"root\": {\"id\":1,\"name\":\"My bean\"}}";

        ObjectReader reader = new ObjectMapper()
                .enable(DeserializationFeature.UNWRAP_ROOT_VALUE)
                .readerFor(Rooted.class);

        Rooted data = reader.readValue(json);

        assertThat(data.id).isEqualTo("1");
        assertThat(data.name).isEqualTo("My bean");
    }

    @JsonRootName(value = "root")
    public static class Rooted {
        public final String id;
        public final String name;

        public Rooted(String id, String name) {
            this.id = id;
            this.name = name;
        }

        @JsonCreator
        public static Rooted create(
                @JsonProperty("id") String id,
                @JsonProperty("type") String names) {
            return new Rooted(id, names);
        }

    }

    @Test
    public void readJsonTree() throws IOException {
        String jsonString = "{\"k1\":{\"value\": \"v1\"},\"k2\":\"v2\"}";

        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.readTree(jsonString);
        assertThat(actualObj.isObject()).isTrue();
        JsonNode k1 = actualObj.get("k1").get("value");
        assertThat(k1.textValue()).isEqualTo("v1");

    }

    @Test
    public void staticCreatorWithDecisionMaking() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectReader reader = objectMapper.readerFor(PolyDataCreator.class);

        PolyDataCreator.Type1 data = reader.readValue("{\"id\":1,\"type\":\"type 1\"}");
        assertThat(data).isEqualTo(new PolyDataCreator.Type1("1"));

        PolyDataCreator.Type2 data2 = reader.readValue("{\"id\":2,\"type\":\"type 2\"}");
        assertThat(data2).isEqualTo(new PolyDataCreator.Type2(2L));

        CollectionType javaType = objectMapper.getTypeFactory().constructCollectionType(List.class, PolyDataCreator.class);

        List<PolyDataCreator> datas = objectMapper.readValue("[{\"id\":3,\"type\":\"type 2\"}, {\"id\":4,\"type\":\"type 1\"}]", javaType);
        assertThat(datas).containsExactly(
                new PolyDataCreator.Type2(3L),
                new PolyDataCreator.Type1("4")
        );
    }

    public static class PolyDataCreator {

        @JsonCreator
        public static PolyDataCreator create(
                @JsonProperty("id") long id,
                @JsonProperty("type") String name) {
            if (name.equals("type 1")) {
                return new Type1(id + "");
            } else {
                return new Type2(id);
            }
        }

        public static class Type1 extends PolyDataCreator {
            final String id;

            public Type1(String id) {
                this.id = id;
            }

            @Override
            public String toString() {
                return "Type1{" +
                        "id='" + id + '\'' +
                        '}';
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Type1 type1 = (Type1) o;
                return Objects.equals(id, type1.id);
            }

            @Override
            public int hashCode() {

                return Objects.hash(id);
            }
        }

        public static class Type2 extends PolyDataCreator {
            final long id;

            public Type2(long id) {
                this.id = id;
            }

            @Override
            public String toString() {
                return "Type2{" +
                        "id=" + id +
                        '}';
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Type2 type2 = (Type2) o;
                return id == type2.id;
            }

            @Override
            public int hashCode() {

                return Objects.hash(id);
            }
        }
    }
}
