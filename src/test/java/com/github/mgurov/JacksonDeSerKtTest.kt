package com.github.mgurov

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonRootName
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Test

import java.io.IOException
import java.util.Objects

import org.assertj.core.api.Assertions.assertThat

class JacksonDeSerKtTest {

    @Test
    @Throws(IOException::class)
    fun simplest() {
        val objectMapper = ObjectMapper()
        val data = objectMapper.readValue("{\"field\": \"value\"}", Data::class.java)
        assertThat(data.field).isEqualTo("value")
    }

    class Data {
        @JsonProperty
        internal var field: String? = null
    }

    @Test
    @Throws(IOException::class)
    fun creator() {
        val reader = ObjectMapper().readerFor(DataCreator::class.java)

        val data = reader.readValue<DataCreator>("{\"id\":1,\"theName\":\"My bean\"}")

        assertThat(data.id).isEqualTo("1")
    }

    class DataCreator @JsonCreator
    constructor(
            @param:JsonProperty("id") internal val id: String,
            @param:JsonProperty("theName") internal val name: String)

    @Test
    @Throws(IOException::class)
    fun staticCreator() {
        val json = "{\"id\":1,\"type\":\"My bean\"}"

        val reader = ObjectMapper()
                //.registerModule(KotlinModule())
                .readerFor(StaticDataCreator::class.java)

        val data = reader.readValue<StaticDataCreator>(json)

        assertThat(data.id).isEqualTo("1")
    }

    class StaticDataCreator(internal val id: String, internal val name: String) {
        companion object {

            @JvmStatic
            @JsonCreator
            fun create(
                    @JsonProperty("id") id: String,
                    @JsonProperty("type") name: String): StaticDataCreator {
                return StaticDataCreator(id, name)
            }
        }
    }

    @Test
    @Throws(IOException::class)
    fun deRootName() {
        val json = "{\"root\": {\"id\":1,\"name\":\"My bean\"}}"

        val reader = ObjectMapper()
                .enable(DeserializationFeature.UNWRAP_ROOT_VALUE)
                .readerFor(Rooted::class.java)

        val data = reader.readValue<Rooted>(json)

        assertThat(data.id).isEqualTo("1")
    }

    @JsonRootName(value = "root")
    data class Rooted(val id: String, val name: String) {
        companion object {

            @JvmStatic
            @JsonCreator
            fun create(
                    @JsonProperty("id") id: String,
                    @JsonProperty("name") name: String): Rooted {
                return Rooted(id, name)
            }
        }

    }

    @Test
    @Throws(IOException::class)
    fun readJsonTree() {
        val jsonString = "{\"k1\":{\"value\": \"v1\"},\"k2\":\"v2\"}"

        val mapper = ObjectMapper()
        val actualObj = mapper.readTree(jsonString)
        assertThat(actualObj.isObject).isTrue()
        val k1 = actualObj.get("k1").get("value")
        assertThat(k1.textValue()).isEqualTo("v1")

    }

    @Test
    @Throws(IOException::class)
    fun staticCreatorWithDecisionMaking() {
        val objectMapper = ObjectMapper()
        val reader = objectMapper.readerFor(PolyDataCreator::class.java)

        val data = reader.readValue<PolyDataCreator.Type1>("{\"id\":1,\"type\":\"type 1\"}")
        assertThat(data).isEqualTo(PolyDataCreator.Type1("1"))

        val data2 = reader.readValue<PolyDataCreator.Type2>("{\"id\":2,\"type\":\"type 2\"}")
        assertThat(data2).isEqualTo(PolyDataCreator.Type2(2L))

        val javaType = objectMapper.typeFactory.constructCollectionType(List::class.java, PolyDataCreator::class.java)

        val datas = objectMapper.readValue<List<PolyDataCreator>>("[{\"id\":3,\"type\":\"type 2\"}, {\"id\":4,\"type\":\"type 1\"}]", javaType)
        assertThat(datas).containsExactly(
                PolyDataCreator.Type2(3L),
                PolyDataCreator.Type1("4")
        )
    }

    open class PolyDataCreator {

        class Type1(internal val id: String) : PolyDataCreator() {

            override fun toString(): String {
                return "Type1{" +
                        "id='" + id + '\''.toString() +
                        '}'.toString()
            }

            override fun equals(o: Any?): Boolean {
                if (this === o) return true
                if (o == null || javaClass != o.javaClass) return false
                val type1 = o as Type1?
                return id == type1!!.id
            }

            override fun hashCode(): Int {

                return Objects.hash(id)
            }
        }

        class Type2(internal val id: Long) : PolyDataCreator() {

            override fun toString(): String {
                return "Type2{" +
                        "id=" + id +
                        '}'.toString()
            }

            override fun equals(o: Any?): Boolean {
                if (this === o) return true
                if (o == null || javaClass != o.javaClass) return false
                val type2 = o as Type2?
                return id == type2!!.id
            }

            override fun hashCode(): Int {

                return Objects.hash(id)
            }
        }

        companion object {

            @JvmStatic
            @JsonCreator
            fun create(
                    @JsonProperty("id") id: Long,
                    @JsonProperty("type") name: String): PolyDataCreator {
                return if (name == "type 1") {
                    Type1(id.toString() + "")
                } else {
                    Type2(id)
                }
            }
        }
    }

}
