package com.github.mgurov.configuration;

import feign.Feign;
import feign.Logger;
import feign.RequestLine;
import feign.Retryer;
import feign.slf4j.Slf4jLogger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SelfClientConfiguration {
    @Bean
    public SelfClient selfClient1(
            @Value("${app.self-url-1:http://localhost:8080}") String url
    ) {
        return buildClient(url);
    }

    @Bean
    public SelfClient selfClient2(
            @Value("${app.self-url-2:http://localhost:8080}") String url
    ) {
        return buildClient(url);
    }

    private SelfClient buildClient(String url) {
        return Feign.builder()
                .retryer(new Retryer.Default(100, 100, 2))
                .logger(new Slf4jLogger(SelfClient.class))
                .logLevel(Logger.Level.FULL)
                .target(SelfClient.class, url);
    }

    public interface SelfClient {
        @RequestLine("GET /boom")
        String boom();
    }


}
