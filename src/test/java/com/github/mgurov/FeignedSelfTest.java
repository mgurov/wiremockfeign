package com.github.mgurov;

import feign.Feign;
import feign.Logger;
import feign.RequestLine;
import feign.slf4j.Slf4jLogger;
import org.assertj.core.api.AutoCloseableSoftAssertions;
import org.junit.Test;

public class FeignedSelfTest {

    @Test
    public void testConnection() {
        FeignClient client = Feign.builder()
                .logger(new Slf4jLogger(FeignClient.class))
                .logLevel(Logger.Level.FULL)
                .target(FeignClient.class, "http://localhost:8080/");

        try (AutoCloseableSoftAssertions softly = new AutoCloseableSoftAssertions()) {
            softly.assertThat(client.stub1()).isEqualTo("stub1");
            softly.assertThat(client.stub2()).isEqualTo("stub2");
        }
    }

    public interface FeignClient {
        @RequestLine("GET /stub1")
        String stub1();

        @RequestLine("GET /stub2")
        String stub2();
    }

}
